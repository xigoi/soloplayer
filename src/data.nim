import
  nimgame2 / [
    assets,
    mosaic,
    scene,
    texturegraphic,
    truetypefont,
    types,
  ]

const
  GameWidth* = 1280
  GameHeight* = 720
  GameDim*: Dim = (1280, 720)
  GameTitle* = "Soloplayer"
  NumberOfLevels* = 2

var
  titleScene*, mainScene*: Scene
  normalFont*, titleFont*: TrueTypeFont
  gfxData*: Assets[TextureGraphic]
  buttonMosaic*: Mosaic
  buttonSkin*: TextureGraphic
  levelNumber*: Positive = 255

proc loadData*() =
  normalFont = newTrueTypeFont()
  discard normalFont.load("data/fnt/FantasqueSansMono-Regular.ttf", 32)
  titleFont = newTrueTypeFont()
  discard titleFont.load("data/fnt/FantasqueSansMono-Bold.ttf", 96)
  gfxData = newAssets[TextureGraphic]("data/gfx", newTextureGraphic)
  buttonMosaic = newMosaic("data/gui/button.png", (8, 8))
  buttonSkin = newTextureGraphic()
  discard buttonSkin.assignTexture buttonMosaic.render(patternStretchBorder(8, 2))

proc freeData*() =
  normalFont.free
  titleFont.free
  for graphic in gfxData.values:
    graphic.free()
