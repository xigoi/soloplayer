import
  nimgame2 / [
    assets,
    entity,
    tilemap,
    types,
  ],
  data,
  level

const
  GravAcc = 1000
  Drag = 400
  JumpVel = 350
  WalkVel = 100
  Size: Dim = (8, 30)

type
  Player* = ref object of Entity
    level*: Level

proc resetPosition*(player: Player) =
  player.pos = player.level.pos + player.level.tilePos(player.level.firstTileIndex(PlayerTile)) + (4, -20).Coord

proc init*(player: Player, level: Level) =
  player.initEntity()
  player.tags.add "player"
  player.level = level
  player.graphic = gfxData["player"]
  player.initSprite(Size)
  player.collider = newBoxCollider(player, Size / 2, Size)
  player.acc.y = GravAcc
  player.drg.x = Drag
  player.physics = platformerPhysics

proc jump*(player: Player) =
  if player.vel.y == 0.0:
    player.vel.y -= JumpVel

proc right*(player: Player, elapsed: float) =
  player.vel.x = WalkVel

proc left*(player: Player, elapsed: float) =
  player.vel.x = -WalkVel

proc newPlayer*(level: Level): Player =
  new result
  result.init(level)

method update*(player: Player, elapsed: float) =
  player.updateEntity elapsed
  # player.updateVisibility()
