import
  nimgame2 / [
    nimgame,
  ],
  data,
  title,
  main

game = newGame()
if game.init(GameWidth, GameHeight, title = GameTitle):
  game.setResizable(true)
  game.windowSize = (GameWidth, GameHeight)
  game.maximize
  game.centrify
  loadData()
  titleScene = newTitleScene()
  mainScene = newMainScene()
  game.scene = titleScene
  game.run
