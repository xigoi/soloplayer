import
  nimgame2 / [
    entity,
    input,
    nimgame,
    scene,
    settings,
    textgraphic,
    tilemap,
    types,
  ],
  data,
  level,
  player

type
  MainScene* = ref object of Scene
    level: Level
    player: Player

proc loadLevel(scene: MainScene, number: Positive) =
  scene.clear
  scene.level = newLevel(number)
  scene.level.pos = (GameDim - scene.level.TileMap.dim) / 2
  scene.add scene.level
  scene.level.layer = 0
  scene.player = newPlayer(scene.level)
  scene.player.collisionEnvironment = @[Entity(scene.level)]
  scene.player.layer = 1
  scene.player.resetPosition()
  scene.add scene.player
  scene.render

proc init*(scene: MainScene) =
  init scene.Scene

proc free*(scene: MainScene) =
  discard

proc newMainScene*(): MainScene =
  new result, free
  init result

method show*(scene: MainScene) =
  if scene.level == nil or scene.level.number != levelNumber:
    scene.loadLevel levelNumber

method event*(scene: MainScene, event: Event) =
  scene.eventScene event
  if event.kind == KeyDown:
    case event.key.keysym.sym:
    of K_F10:
      colliderOutline = not colliderOutline
    of K_F11:
      showInfo = not showInfo
    else: discard

method update*(scene: MainScene, elapsed: float) =
  scene.updateScene(elapsed)
  if ScancodeUp.pressed or ScancodeW.pressed:
    scene.player.jump
  if ScancodeRight.down or ScancodeD.down:
    scene.player.right(elapsed)
  if ScancodeLeft.down or ScancodeA.down:
    scene.player.left(elapsed)
  if ScanCodeSpace.pressed:
    scene.level.switchSecretTiles
    if scene.level.collider.collide(scene.player.collider):
      scene.level.switchSecretTiles
  if ScancodeEscape.pressed:
    game.scene = titleScene
