import
  nimgame2 / [
    entity,
    gui/button,
    gui/widget,
    input,
    nimgame,
    scene,
    settings,
    textgraphic,
  ],
  data

type
  TitleScene = ref object of Scene

proc init*(scene: TitleScene) =
  init scene.Scene

  let titleText = newTextGraphic(titleFont)
  titleText.setText GameTitle
  let title = newEntity()
  title.graphic = titleText
  title.centrify
  title.pos = (GameWidth / 2, GameHeight / 3)
  scene.add title

  let playButtonLabel = newTextGraphic(normalFont)
  playButtonLabel.setText "PLAY"
  let playButton = newGuiButton(buttonSkin, playButtonLabel)
  playButton.centrify
  playButton.pos = (GameWidth / 2, GameHeight / 2)
  playButton.actions.add proc(widget: GuiWidget) =
    game.scene = mainScene
  scene.add playButton

  let exitButtonLabel = newTextGraphic(normalFont)
  exitButtonLabel.setText "EXIT"
  let exitButton = newGuiButton(buttonSkin, exitButtonLabel)
  exitButton.centrify
  exitButton.pos = (GameWidth / 2, GameHeight / 2 + 64)
  exitButton.mbAllow = left
  exitButton.enable
  exitButton.actions.add proc(widget: GuiWidget) =
    gameRunning = false
  scene.add exitButton

proc free*(scene: TitleScene) =
  discard

proc newTitleScene*(): TitleScene =
  new result, free
  init result

method update*(scene: TitleScene, elapsed: float) =
  scene.updateScene(elapsed)
  if Scancode1.pressed:
    levelNumber = 1
    game.scene = mainScene
  if Scancode2.pressed:
    levelNumber = 2
    game.scene = mainScene
  if ScancodeEscape.pressed:
    gameRunning = false
