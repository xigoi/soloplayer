import
  strformat,
  strutils,
  sugar,
  tables,
  nimgame2 / [
    assets,
    entity,
    tilemap,
  ],
  data

const
  TileDim* = (16, 16)
  AirTile* = 0
  WallTile* = 1
  PlayerTile* = 2
  SecretTileClosed* = 3
  SecretTileOpen* = 4
  Button0TileUnpressed* = 5
  Button0TilePressed* = 6
  Door0TileClosed* = 7
  Door0TileOpen* = 8
  tilesByName = {
    ' ': AirTile,
    '#': WallTile,
    'P': PlayerTile,
    '*': SecretTileClosed,
    'B': Button0TileUnpressed,
    'D': Door0TileClosed,
  }.toTable
  PassableTiles = @[
    AirTile,
    PlayerTile,
    SecretTileOpen,
    Button0TileUnpressed,
    Button0TilePressed,
    Door0TileOpen
  ]

type
  Level* = ref object of TileMap
    number*: Positive

proc init*(level: Level, number: Positive) =
  init Tilemap level
  level.tags.add("level")
  level.graphic = gfxData["tiles"]
  level.initSprite(TileDim)
  level.map = collect(newSeq):
    for line in lines(&"data/levels/{number}.txt"):
      collect(newSeq):
        for ch in line:
          tilesByName[ch]
  level.passable = PassableTiles
  level.initCollider

proc switchSecretTiles*(level: Level) =
  for row in level.map.mitems:
    for tile in row.mitems:
      case tile
      of SecretTileClosed:
        tile = SecretTileOpen
      of SecretTileOpen:
        tile = SecretTileClosed
      else: discard
  level.initCollider

proc newLevel*(number: Positive): Level =
  result = Level(number: number)
  result.init(number)
